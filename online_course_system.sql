DROP DATABASE IF EXISTS online_course_system;

CREATE DATABASE online_course_system;

USE online_course_system;

DROP TABLE IF EXISTS 	`course`;
CREATE TABLE IF NOT EXISTS `course` (
	id 				INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
	course_name 	NVARCHAR(50) NOT NULL UNIQUE KEY,
    course_price	INT	UNSIGNED,
    total_member 	INT UNSIGNED,
    content 		TEXT,
    created_date	DATETIME DEFAULT NOW()
);


-- create table: Account
DROP TABLE IF EXISTS `account`;
CREATE TABLE `account`(
	id				INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    username		VARCHAR(50) NOT NULL UNIQUE,
	`password` 		VARCHAR(100) NOT NULL,
    first_name		NVARCHAR(50) NOT NULL,
    last_name		NVARCHAR(50) NOT NULL,
    email			NVARCHAR(50) NOT NULL UNIQUE,
    avatar_url		NVARCHAR(250),
    `role` 			ENUM('ADMIN','STUDENT','SUPPORT', 'LECTURER') NOT NULL DEFAULT 'STUDENT',
    `status`		TINYINT DEFAULT 0 -- 0: Not Active, 1: Active, 2: Block
);

DROP TABLE IF EXISTS 	`course_account`;
CREATE TABLE IF NOT EXISTS `course_account` (
	id 				INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
	course_id 		INT UNSIGNED,
    account_id		INT	UNSIGNED,
    mark 			TINYINT UNSIGNED,
    CONSTRAINT fk_course
    FOREIGN KEY (course_id) 
    REFERENCES `course`(id) 
		ON DELETE SET NULL 
        ON UPDATE CASCADE,
    CONSTRAINT fk_account 
    FOREIGN KEY (account_id) 
    REFERENCES `account`(id) 
		ON DELETE SET NULL 
		ON UPDATE CASCADE,
	CONSTRAINT `unique_key_course_account` UNIQUE (course_id,account_id)
);

-- Create table Registration_Account_Token
DROP TABLE IF EXISTS `Registration_Account_Token`;
CREATE TABLE `Registration_Account_Token`(
	id 						INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    token 					CHAR(36) NOT NULL UNIQUE KEY,
	account_id				INT UNSIGNED NOT NULL,
    expiry_date				DATETIME NOT NULL,
    CONSTRAINT fk_register_account
    FOREIGN KEY (account_id) 
    REFERENCES `account`(id) 
		ON DELETE CASCADE 
		ON UPDATE CASCADE
);

-- Create table Reset_Password_Token
DROP TABLE IF EXISTS 	`Reset_Password_Token`;
CREATE TABLE IF NOT EXISTS `Reset_Password_Token` ( 	
	id 				INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
	`token`	 		CHAR(36) NOT NULL UNIQUE,
	`account_id` 	INT UNSIGNED NOT NULL,
	`expiryDate` 	DATETIME NOT NULL,
    CONSTRAINT fk_reset_password_account
    FOREIGN KEY (account_id)
    REFERENCES `account`(id) 
		ON DELETE CASCADE 
		ON UPDATE CASCADE
);

-- insert data

insert into `account` (username, `password`, first_name, last_name, email, `role`, status) values ('tedbrooke0', '$2a$10$cfnp4vogz79KIi/bg68ia.k7FqKcFOIgZE/jCoRqm99BSs36aKHZG', 'Torey', 'Edbrooke', 'tedbrooke0@wordpress.org', 'LECTURER', 0);
insert into `account` (username, `password`, first_name, last_name, email, `role`, status) values ('phardbattle1', '$2a$10$cfnp4vogz79KIi/bg68ia.k7FqKcFOIgZE/jCoRqm99BSs36aKHZG', 'Peder', 'Hardbattle', 'phardbattle1@t.co', 'STUDENT', 1);
insert into `account` (username, `password`, first_name, last_name, email, `role`, status) values ('prykert2', '$2a$10$cfnp4vogz79KIi/bg68ia.k7FqKcFOIgZE/jCoRqm99BSs36aKHZG', 'Pansie', 'Rykert', 'prykert2@scientificamerican.com', 'LECTURER', 0);
insert into `account` (username, `password`, first_name, last_name, email, `role`, status) values ('abenardette3', '$2a$10$cfnp4vogz79KIi/bg68ia.k7FqKcFOIgZE/jCoRqm99BSs36aKHZG', 'Adrien', 'Benardette', 'abenardette3@lycos.com', 'LECTURER', 0);
insert into `account` (username, `password`, first_name, last_name, email, `role`, status) values ('cfippe4', '$2a$10$cfnp4vogz79KIi/bg68ia.k7FqKcFOIgZE/jCoRqm99BSs36aKHZG', 'Chev', 'Fippe', 'cfippe4@w3.org', 'STUDENT', 1);
insert into `account` (username, `password`, first_name, last_name, email, `role`, status) values ('fortler5', '$2a$10$cfnp4vogz79KIi/bg68ia.k7FqKcFOIgZE/jCoRqm99BSs36aKHZG', 'Foss', 'Ortler', 'fortler5@redcross.org', 'STUDENT', 0);
insert into `account` (username, `password`, first_name, last_name, email, `role`, status) values ('lbaukham6', '$2a$10$cfnp4vogz79KIi/bg68ia.k7FqKcFOIgZE/jCoRqm99BSs36aKHZG', 'Laurice', 'Baukham', 'lbaukham6@cmu.edu', 'STUDENT', 1);
insert into `account` (username, `password`, first_name, last_name, email, `role`, status) values ('abartrum7', '$2a$10$cfnp4vogz79KIi/bg68ia.k7FqKcFOIgZE/jCoRqm99BSs36aKHZG', 'Ardyce', 'Bartrum', 'abartrum7@pen.io', 'ADMIN', 0);
insert into `account` (username, `password`, first_name, last_name, email, `role`, status) values ('kslader8', '$2a$10$cfnp4vogz79KIi/bg68ia.k7FqKcFOIgZE/jCoRqm99BSs36aKHZG', 'Krissie', 'Slader', 'kslader8@blogtalkradio.com', 'STUDENT', 0);
insert into `account` (username, `password`, first_name, last_name, email, `role`, status) values ('cwillson9', '$2a$10$cfnp4vogz79KIi/bg68ia.k7FqKcFOIgZE/jCoRqm99BSs36aKHZG', 'Caril', 'Willson', 'cwillson9@hostgator.com', 'ADMIN', 1);
insert into `account` (username, `password`, first_name, last_name, email, `role`, status) values ('gdroghana', '$2a$10$cfnp4vogz79KIi/bg68ia.k7FqKcFOIgZE/jCoRqm99BSs36aKHZG', 'Gregoire', 'Droghan', 'gdroghana@adobe.com', 'LECTURER', 1);
insert into `account` (username, `password`, first_name, last_name, email, `role`, status) values ('tweallb', '$2a$10$cfnp4vogz79KIi/bg68ia.k7FqKcFOIgZE/jCoRqm99BSs36aKHZG', 'Tasia', 'Weall', 'tweallb@toplist.cz', 'SUPPORT', 1);
insert into `account` (username, `password`, first_name, last_name, email, `role`, status) values ('smaltsterc', '$2a$10$cfnp4vogz79KIi/bg68ia.k7FqKcFOIgZE/jCoRqm99BSs36aKHZG', 'Sandy', 'Maltster', 'smaltsterc@imdb.com', 'STUDENT', 1);
insert into `account` (username, `password`, first_name, last_name, email, `role`, status) values ('jbattershalld', '$2a$10$cfnp4vogz79KIi/bg68ia.k7FqKcFOIgZE/jCoRqm99BSs36aKHZG', 'Jules', 'Battershall', 'jbattershalld@google.com', 'STUDENT', 0);
insert into `account` (username, `password`, first_name, last_name, email, `role`, status) values ('sbendane', '$2a$10$cfnp4vogz79KIi/bg68ia.k7FqKcFOIgZE/jCoRqm99BSs36aKHZG', 'Saraann', 'Bendan', 'sbendane@opensource.org', 'STUDENT', 0);
insert into `account` (username, `password`, first_name, last_name, email, `role`, status) values ('wtattamf', '$2a$10$cfnp4vogz79KIi/bg68ia.k7FqKcFOIgZE/jCoRqm99BSs36aKHZG', 'Warden', 'Tattam', 'wtattamf@aol.com', 'STUDENT', 1);
insert into `account` (username, `password`, first_name, last_name, email, `role`, status) values ('vfealyg', '$2a$10$cfnp4vogz79KIi/bg68ia.k7FqKcFOIgZE/jCoRqm99BSs36aKHZG', 'Verile', 'Fealy', 'vfealyg@list-manage.com', 'SUPPORT', 0);
insert into `account` (username, `password`, first_name, last_name, email, `role`, status) values ('cvanyushinh', '$2a$10$cfnp4vogz79KIi/bg68ia.k7FqKcFOIgZE/jCoRqm99BSs36aKHZG', 'Cristobal', 'Vanyushin', 'cvanyushinh@accuweather.com', 'STUDENT', 0);
insert into `account` (username, `password`, first_name, last_name, email, `role`, status) values ('mjudkini', '$2a$10$cfnp4vogz79KIi/bg68ia.k7FqKcFOIgZE/jCoRqm99BSs36aKHZG', 'Mackenzie', 'Judkin', 'mjudkini@bloomberg.com', 'STUDENT', 1);
insert into `account` (username, `password`, first_name, last_name, email, `role`, status) values ('ptownendj', '$2a$10$cfnp4vogz79KIi/bg68ia.k7FqKcFOIgZE/jCoRqm99BSs36aKHZG', 'Persis', 'Townend', 'ptownendj@lulu.com', 'SUPPORT', 1);

-- course
insert into `course` (course_name, course_price, total_member, content, created_date) values ('HTML', 191179, 38, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', '2021-12-27');
insert into `course` (course_name, course_price, total_member, content, created_date) values ('Java', 240224, 47, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', '2021-07-11');
insert into `course` (course_name, course_price, total_member, content, created_date) values ('Javascript', 430850, 34, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', '2022-04-18');
insert into `course` (course_name, course_price, total_member, content, created_date) values ('CSS', 121328, 35, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', '2022-04-13');
insert into `course` (course_name, course_price, total_member, content, created_date) values ('C#', 240914, 42, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', '2021-08-09');