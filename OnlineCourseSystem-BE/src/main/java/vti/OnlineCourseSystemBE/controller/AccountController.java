package vti.OnlineCourseSystemBE.controller;

import org.json.JSONObject;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import vti.OnlineCourseSystemBE.dto.AccountDTO;
import vti.OnlineCourseSystemBE.entity.Account;
import vti.OnlineCourseSystemBE.form.AccountFilterForm;
import vti.OnlineCourseSystemBE.form.AccountUpdatingForm;
import vti.OnlineCourseSystemBE.form.ChangePasswordForm;
import vti.OnlineCourseSystemBE.form.CreateAccountForm;
import vti.OnlineCourseSystemBE.service.IAccountService;

import java.util.List;

@CrossOrigin("*")
@Validated
@RestController
@RequestMapping(value = "/api/account/")
public class AccountController {

    @Autowired
    private IAccountService accountService;

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private JSONObject jsonObject;


    @GetMapping("paging-account")
    public ResponseEntity<?> getPagingAccounts(Pageable pageable,
                                              @RequestParam(value = "search", required = false) String search,
                                              AccountFilterForm filterForm) {

        Page<Account> pageAccount = accountService.getPagingAccounts(pageable, search, filterForm);

        System.out.println(pageAccount.toString());
        List<AccountDTO> listAccountDTO = modelMapper.map(pageAccount.getContent(),
                new TypeToken<List<AccountDTO>>(){}.getType());
        Page<AccountDTO> pageAccountDTO = new PageImpl<>(listAccountDTO, pageable, pageAccount.getTotalElements());

        return new  ResponseEntity<>(pageAccountDTO,HttpStatus.OK);
    }

    @PostMapping("create")
    public ResponseEntity<?> createAccount(@RequestBody CreateAccountForm createAccountForm) {
        Account account = modelMapper.map(createAccountForm, Account.class);
        accountService.createAccount(account);
        return new ResponseEntity<>("Create Account Successfully",HttpStatus.OK);
    }

    @PostMapping("delete/{username}")
    public ResponseEntity<?> deleteAccount(@PathVariable(name = "username") String username) {
        accountService.deleteAccount(username);

        jsonObject.put("resultText: ", "Account deleted");
        jsonObject.put("status", 200);
        return new ResponseEntity<>(jsonObject.toString(), HttpStatus.OK);
    }

    /**
     *  FOR ALL ROLE
     * **/

    @GetMapping("view-profile/{username}")
    public ResponseEntity<?> getAccountInfoByUsername(@PathVariable(name="username") String username) {
        Account account = accountService.findAccountByUsername(username);

        AccountDTO accountInfo = modelMapper.map(account, AccountDTO.class);

        return new ResponseEntity<>(accountInfo,HttpStatus.OK);
    }

    @PutMapping("update-profile/")
    public ResponseEntity<?> updateAccountInfoByUsername(@RequestBody AccountUpdatingForm updateAccountFrom) {
        Account account = modelMapper.map(updateAccountFrom, Account.class);
        accountService.updateAccountInfoByUsername(account);

        jsonObject.put("resultText: ", "Account updated successfully");
        jsonObject.put("status", 200);
        return new ResponseEntity<>(jsonObject.toString(), HttpStatus.OK);
    }

    @PutMapping("change-password/{username}")
    public ResponseEntity<?> changePassword(@PathVariable(name="username") String username,
                                            @RequestBody ChangePasswordForm form) {
        Account account = accountService.findAccountByUsername(username);

        if (account.getPassword().equals(form.getOldPassword())) {
            return new ResponseEntity<>("Password is incorrect", HttpStatus.BAD_REQUEST);
        }
        accountService.changePassword(account, form.getNewPassword());

        jsonObject.put("resultText: ", "Password has changed");
        jsonObject.put("status", 200);
        return new ResponseEntity<>(jsonObject.toString(), HttpStatus.OK);
    }
}
