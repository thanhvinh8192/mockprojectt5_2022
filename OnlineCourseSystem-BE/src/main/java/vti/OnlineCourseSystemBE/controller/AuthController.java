package vti.OnlineCourseSystemBE.controller;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import vti.OnlineCourseSystemBE.config.security.JwtUtils;
import vti.OnlineCourseSystemBE.dto.JWTResponseDTO;
import vti.OnlineCourseSystemBE.dto.RegisterDTO;
import vti.OnlineCourseSystemBE.dto.SignInDTO;
import vti.OnlineCourseSystemBE.entity.Account;
import vti.OnlineCourseSystemBE.service.IAuthService;
import vti.OnlineCourseSystemBE.service.UserDetailsImpl;

import javax.validation.Valid;

@CrossOrigin("*")
@Validated
@RestController
@RequestMapping(value = "/api/auth/")
public class AuthController {

    @Autowired
    private IAuthService authService;

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    JwtUtils jwtUtils;

    @PostMapping(value = "sign-in")
    public ResponseEntity<?> signIn(@RequestBody SignInDTO signIn) {
        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(signIn.getUsername(), signIn.getPassword())
        );

        SecurityContextHolder.getContext().setAuthentication(authentication);
        UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
        String token = jwtUtils.generateJwtToken(userDetails);

        return ResponseEntity.ok(new JWTResponseDTO(
                token,
                userDetails.getUsername(),
                userDetails.getAuthorities().toString(),
                userDetails.getStatus(),
                userDetails.getEmail(),
                userDetails.getFirstName(),
                userDetails.getLastName()
                ));
    }

    @PostMapping(value = "sign-up")
    public ResponseEntity<?> register(@Valid @RequestBody RegisterDTO register) {
        Account account = modelMapper.map(register, Account.class);
        authService.registerAccount(account);
        return new ResponseEntity<>("We have sent 1 email. Please check email to active account!", HttpStatus.OK);
    }

    @GetMapping("active-account")
    public ResponseEntity<?> activeAccountViaEmail(@RequestParam String token) {

        authService.activeAccount(token);

        return new ResponseEntity<>("Active success!", HttpStatus.OK);
    }

    @GetMapping("email/{email}")
    public ResponseEntity<?> existAccountByEmail(@PathVariable(name = "email") String email) {
        boolean result = authService.existAccountByEmail(email);

        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @GetMapping("username/{username}")
    public ResponseEntity<?> existAccountByUsername(@PathVariable(name = "username") String username) {
        boolean result = authService.existAccountByUsername(username);

        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @GetMapping(value = "forgot-password")
    public ResponseEntity<?> forgotPassword(@RequestParam String email) {
        authService.forgotPassword(email);

        return new ResponseEntity<>("We have sent 1 email. Please check email to reset password account!", HttpStatus.OK);
    }

    @GetMapping("/reset-password")
    public ResponseEntity<?> resetPasswordViaEmail(@RequestParam String token, @RequestParam String newPassword) {

        // reset password
        authService.resetPassword(token, newPassword);

        return new ResponseEntity<>("Reset Password success!", HttpStatus.OK);
    }
}
