package vti.OnlineCourseSystemBE.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


import vti.OnlineCourseSystemBE.form.CourseFilterForm;

import vti.OnlineCourseSystemBE.form.CourseFormForCreating;
import vti.OnlineCourseSystemBE.form.CourseFormForUpdating;
import vti.OnlineCourseSystemBE.entity.Course;
import vti.OnlineCourseSystemBE.service.ICourseService;



@RestController
@RequestMapping(value = "api/courses")
public class CourseController {

	@Autowired
	private ICourseService service;

	@GetMapping()
	public ResponseEntity<?> getAllCourses(
			Pageable pageable, 
			CourseFilterForm filter,
			@RequestParam(required = false) 
			String search) {
		Page<Course> entities = service.getAllCourses(pageable, filter, search);
		return new ResponseEntity<>(entities, HttpStatus.OK);
	}

	@GetMapping(value = "/name/{name}")
	public ResponseEntity<?> existsCourseByName(@PathVariable(name = "name") String name) {
		return new ResponseEntity<>(service.isCourseExistsByName(name), HttpStatus.OK);
	}

	@PostMapping()
	public ResponseEntity<?> createCourse(@RequestBody CourseFormForCreating form) {
		service.createCourse(form);
		return new ResponseEntity<String>("Create successfully!", HttpStatus.OK);
	}

	@GetMapping(value = "/{id}")
	public ResponseEntity<?> getCourseByID(@PathVariable(name = "id") short id) {
		return new ResponseEntity<>(service.getCourseByID(id), HttpStatus.OK);
	}

	@PutMapping(value = "/{id}")
	public ResponseEntity<?> updateCourse(@PathVariable(name = "id") short id, @RequestBody CourseFormForUpdating form) {
		service.updateCourse(id, form);
		return new ResponseEntity<String>("Update successfully!", HttpStatus.OK);
	}

	@DeleteMapping(value = "/{ids}")
	public ResponseEntity<?> deleteCourses(@PathVariable(name = "ids") List<Short> ids) {
		service.deleteCourse(ids);
		return new ResponseEntity<String>("Delete successfully!", HttpStatus.OK);
	}
}
