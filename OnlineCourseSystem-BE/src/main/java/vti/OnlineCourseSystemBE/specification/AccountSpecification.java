package vti.OnlineCourseSystemBE.specification;

import org.springframework.data.jpa.domain.Specification;
import org.springframework.util.StringUtils;
import vti.OnlineCourseSystemBE.entity.Account;
import vti.OnlineCourseSystemBE.form.AccountFilterForm;

public class AccountSpecification {
    public static Specification<Account> buildWhere(String search, AccountFilterForm filterForm) {
        Specification<Account> where = null;
        if (!StringUtils.isEmpty(search)) {
            search = search.trim();
            AccountCustomSpecification username = new AccountCustomSpecification("username", search);
            AccountCustomSpecification firstName = new AccountCustomSpecification("firstName", search);
            AccountCustomSpecification lastName = new AccountCustomSpecification("lastName", search);
            AccountCustomSpecification email = new AccountCustomSpecification("email", search);

            where = Specification.where(username).or(firstName).or(lastName).or(email);
        }
        if (filterForm != null && !StringUtils.isEmpty(filterForm.getRole())) {
            AccountCustomSpecification accountRole = new AccountCustomSpecification("role", filterForm.getRole());
            if (where == null) where = accountRole;
            else  where = where.and(accountRole);
        }
        return where;
    }
}
