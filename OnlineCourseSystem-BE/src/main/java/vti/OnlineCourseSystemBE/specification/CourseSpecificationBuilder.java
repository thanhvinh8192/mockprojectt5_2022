package vti.OnlineCourseSystemBE.specification;

import org.springframework.data.jpa.domain.Specification;
import org.springframework.util.StringUtils;

import vti.OnlineCourseSystemBE.form.CourseFilterForm;

import vti.OnlineCourseSystemBE.entity.Course;


public class CourseSpecificationBuilder {

	private CourseFilterForm filter;
	private String search;

	public CourseSpecificationBuilder(CourseFilterForm filter, String search) {
		this.filter = filter;
		this.search = search;
	}

	@SuppressWarnings("deprecation")
	public Specification<Course> build() {

		SearchCriteria seachCriteria = new SearchCriteria("name", "Like", search);
		SearchCriteria minTotalMemberCriteria = new SearchCriteria("totalMember", ">=", filter.getMinTotalMember());
		SearchCriteria maxTotalMemberCriteria = new SearchCriteria("totalMember", "<=", filter.getMaxTotalMember());

		Specification<Course> where = null;

		// search
		if (!StringUtils.isEmpty(search)) {
			where = new CourseSpecification(seachCriteria);
		}

		// min totalMember filter
		if (filter.getMinTotalMember() != 0) {
			if (where != null) {
				where = where.and(new CourseSpecification(minTotalMemberCriteria));
			} else {
				where = new CourseSpecification(minTotalMemberCriteria);
			}
		}

		// max totalMember filter
		if (filter.getMaxTotalMember() != 0) {
			if (where != null) {
				where = where.and(new CourseSpecification(maxTotalMemberCriteria));
			} else {
				where = new CourseSpecification(maxTotalMemberCriteria);
			}
		}

		return where;
	}
}
