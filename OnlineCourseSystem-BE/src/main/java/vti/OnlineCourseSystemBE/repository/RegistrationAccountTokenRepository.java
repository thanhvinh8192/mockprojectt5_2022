package vti.OnlineCourseSystemBE.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import vti.OnlineCourseSystemBE.entity.RegistrationAccountToken;

public interface RegistrationAccountTokenRepository extends JpaRepository<RegistrationAccountToken, Integer> {

    RegistrationAccountToken findByToken(String token);

    @Query("	SELECT 	token	"
            + "	FROM 	RegistrationAccountToken "
            + " WHERE 	account.id = :accountId")
    String findByAccountId(int accountId);
}
