package vti.OnlineCourseSystemBE.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import vti.OnlineCourseSystemBE.entity.Course;



public interface CourseRepository extends JpaRepository<Course, Short>, JpaSpecificationExecutor<Course> {

	public Course findByName(String name);

	public boolean existsByName(String name);

	public void deleteByIdIn(List<Short> ids);
}
