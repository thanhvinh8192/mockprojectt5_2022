package vti.OnlineCourseSystemBE.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;
import vti.OnlineCourseSystemBE.entity.ResetPasswordToken;

public interface ResetPasswordTokenRepository extends JpaRepository<ResetPasswordToken, Integer> {

    ResetPasswordToken findByToken(String token);

    @Query("	SELECT 	token	"
            + "	FROM 	ResetPasswordToken "
            + " WHERE 	account.id = :accountId")
    String findByAccountId(int accountId);

    @Transactional
    @Modifying
    @Query("	DELETE 						"
            + "	FROM 	ResetPasswordToken 	"
            + " WHERE 	account.id = :accountId")
    void deleteByAccountId(int accountId);
}
