package vti.OnlineCourseSystemBE.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;
import vti.OnlineCourseSystemBE.entity.Account;

@Repository
public interface AccountRepository extends JpaRepository<Account, Integer>, JpaSpecificationExecutor<Account> {

    Account findByUsername(String username);
    Account findByEmail(String email);

    boolean existsByEmail(String email);

    boolean existsByUsername(String username);
}
