package vti.OnlineCourseSystemBE.entity;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Value;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "`Registration_Account_Token`")
@Getter @Setter
@NoArgsConstructor
public class RegistrationAccountToken implements Serializable {

    private static final long serialVersionUID = 1L;


    private static final long EXPIRATION_TIME = 864000000L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "`id`", unique = true, nullable = false)
    private int id;

    @Column(name = "`token`", nullable = false, length = 36, unique = true)
    private String token;

    @OneToOne(targetEntity = Account.class, fetch = FetchType.EAGER)
    @JoinColumn(nullable = false, name = "account_id")
    private Account account;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "`expiry_date`", nullable = false)
    private Date expiryDate = new Date(System.currentTimeMillis() + EXPIRATION_TIME);;

    public RegistrationAccountToken(String token, Account account) {
        this.token = token;
        this.account = account;
        //expiryDate = new Date(System.currentTimeMillis() + EXPIRATION_TIME);
    }


}
