package vti.OnlineCourseSystemBE.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "`account`")
@Getter
@Setter
@NoArgsConstructor
public class Account {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "`id`", unique = true, nullable = false)
    private int id;

    @Column(name = "`username`", nullable = false, length = 50, unique = true)
    private String username;

    @Column(name = "`first_name`", nullable = false, length = 50)
    private String firstName;

    @Column(name = "`last_name`", nullable = false, length = 50)
    private String lastName;

    @Column(name = "`email`", nullable = false, length = 50, unique = true)
    private String email;

    @Column(name = "`password`", nullable = false, length = 100)
    private String password;

    @Column(name = "`avatar_url`", length = 250)
    private String avatarUrl;

    @Column(name = "role", columnDefinition = "ENUM('ADMIN','STUDENT','SUPPORT', 'LECTURER')")
    @Enumerated(EnumType.STRING)
    private AccountRole role;

    @Enumerated(EnumType.ORDINAL)
    @Column(name = "`status`", nullable = false)
    private AccountStatus status = AccountStatus.NOT_ACTIVE;

    @OneToMany(mappedBy ="account")
    private List<CourseOrder> listCourseOrder;

    /**
     * ENUM ROLE, STATUS
     * **/

    public enum AccountRole {
        ADMIN, STUDENT, SUPPORT, LECTURER;
        public static AccountRole toEnum(String name) {
            for (AccountRole item : AccountRole.values()) {
                if (item.toString().equals(name))
                    return item;
            }
            return null;
        }
    }

    public enum AccountStatus {
        NOT_ACTIVE, ACTIVE, BLOCK
    }

}
