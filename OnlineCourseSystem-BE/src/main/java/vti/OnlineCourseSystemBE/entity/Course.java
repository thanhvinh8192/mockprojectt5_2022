package vti.OnlineCourseSystemBE.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;

import javax.persistence.*;

@Entity
@Getter @Setter
@NoArgsConstructor
@Table(name = "`course`")
public class Course implements Serializable {

	private static final long serialVersionUID = 1L;

	@Column(name = "id")
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@Column(name = "course_name", length = 50, nullable = false, unique = true)
	private String name;
	
	@Column(name = "course_price")
	private int price;

//	@Column(name = "total_member")
//	private int totalMember;
	
	@Column(name = "content")
	private String content;

	@OneToMany(mappedBy ="course")
	private List<CourseOrder> listMember;

	@OneToMany(mappedBy ="course")
	private List<Lesson> lessons;

	@ManyToOne
	@JoinColumn(name = "category_id")
	private Category category;

	public Course(String name, int price, String content) {
		this.name = name;
		this.price = price;
		this.content = content;
	}
}
