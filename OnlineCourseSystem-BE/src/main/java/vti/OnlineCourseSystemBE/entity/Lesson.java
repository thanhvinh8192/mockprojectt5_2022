package vti.OnlineCourseSystemBE.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter @Setter
@NoArgsConstructor
@Table(name = "`lesson`")
public class Lesson {

    @Column(name = "id")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "lesson_name", length = 50, nullable = false)
    private  String lessonName;

    @Column(name = "content")
    private  String content;

    @ManyToOne
    @JoinColumn(name = "course_id")
    private Course course;

}
