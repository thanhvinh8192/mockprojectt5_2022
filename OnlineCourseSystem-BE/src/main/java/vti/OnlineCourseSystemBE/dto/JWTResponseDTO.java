package vti.OnlineCourseSystemBE.dto;

import lombok.*;
import vti.OnlineCourseSystemBE.entity.Account;

@Getter
@Setter
@AllArgsConstructor
public class JWTResponseDTO {


    private String token;

    private String username;

    private String role;

    private String status;

    private String email;

    private String firstName;

    private String lastName;

}
