package vti.OnlineCourseSystemBE.dto;

import lombok.*;
import vti.OnlineCourseSystemBE.entity.Account;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class RegisterDTO {
    private String username;
    private String firstName;
    private String lastName;
    private String email;
    private String password;
    private Account.AccountRole role = Account.AccountRole.STUDENT;
}
