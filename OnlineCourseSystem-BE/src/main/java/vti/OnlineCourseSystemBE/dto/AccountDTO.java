package vti.OnlineCourseSystemBE.dto;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AccountDTO {

    private int id;

    private String username;

    private String password;

    private String firstName;

    private String lastName;

    private String email;

    private String role;

    private String status;
}
