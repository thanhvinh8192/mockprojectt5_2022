package vti.OnlineCourseSystemBE.event;

import org.springframework.context.ApplicationEvent;

public class OnSendRegistrationConfirmEmailEvent extends ApplicationEvent {

    private String email;

    public OnSendRegistrationConfirmEmailEvent(String email) {
        super(email);
        this.email = email;
    }

    public String getEmail() {
        return email;
    }
}
