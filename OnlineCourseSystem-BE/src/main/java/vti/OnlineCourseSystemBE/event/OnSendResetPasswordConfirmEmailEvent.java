package vti.OnlineCourseSystemBE.event;

import lombok.Getter;
import lombok.Setter;
import org.springframework.context.ApplicationEvent;


public class OnSendResetPasswordConfirmEmailEvent extends ApplicationEvent {

    private String email;

    public OnSendResetPasswordConfirmEmailEvent(String email) {
        super(email);
        this.email = email;
    }

    public String getEmail() {
        return email;
    }
}
