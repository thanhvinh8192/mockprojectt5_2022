package vti.OnlineCourseSystemBE.event;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;
import vti.OnlineCourseSystemBE.service.IEmailService;

@Component
public class SendResetPasswordConfirmViaEmailListener implements ApplicationListener<OnSendResetPasswordConfirmEmailEvent> {

    @Autowired
    private IEmailService emailService;

    @Override
    public void onApplicationEvent(OnSendResetPasswordConfirmEmailEvent event) {
        sendResetPasswordViaEmail(event.getEmail());
    }

    private void sendResetPasswordViaEmail(String email) {
        emailService.sendResetPassword(email);
    }
}
