package vti.OnlineCourseSystemBE.event;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;
import vti.OnlineCourseSystemBE.service.IEmailService;

@Component
public class SendRegistrationAccountConfirmViaEmailListener implements ApplicationListener<OnSendRegistrationConfirmEmailEvent> {

    @Autowired
    private IEmailService emailService;

    @Override
    public void onApplicationEvent(OnSendRegistrationConfirmEmailEvent event) {
        sendConfirmViaEmail(event.getEmail());
    }
    private void sendConfirmViaEmail(String email) {
        emailService.sendRegistrationAccountConfirmation(email);
    }
}
