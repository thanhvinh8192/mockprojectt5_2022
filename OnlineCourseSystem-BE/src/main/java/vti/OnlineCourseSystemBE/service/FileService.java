package vti.OnlineCourseSystemBE.service;

import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import vti.OnlineCourseSystemBE.utils.FileManager;

import java.io.IOException;
import java.util.Date;

@Service
public class FileService  implements IFileService{

    private FileManager fileManager = new FileManager();
    private String linkFolder = "/Users/nguyenthanhvinh/Desktop/MockT5_2022/public/avatar";

    @Override
    public String uploadImage(MultipartFile image) throws IOException {

        String nameImage = new Date().getTime() + "." + fileManager.getFormatFile(image.getOriginalFilename());
        String path = linkFolder + "/" + nameImage;

        fileManager.createNewMultiPartFile(path, image);

        return path;
    }
}
