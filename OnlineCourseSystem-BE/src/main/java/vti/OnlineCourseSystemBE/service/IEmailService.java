package vti.OnlineCourseSystemBE.service;

public interface IEmailService {

    void sendRegistrationAccountConfirmation(String email);

    void sendResetPassword(String email);
}
