package vti.OnlineCourseSystemBE.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import vti.OnlineCourseSystemBE.form.CourseFilterForm;

import vti.OnlineCourseSystemBE.form.CourseFormForCreating;
import vti.OnlineCourseSystemBE.form.CourseFormForUpdating;
import vti.OnlineCourseSystemBE.entity.Course;
import vti.OnlineCourseSystemBE.repository.CourseRepository;
import vti.OnlineCourseSystemBE.specification.CourseSpecificationBuilder;


@Service
public class CourseService implements ICourseService {

	@Autowired
	private CourseRepository repository;

	public Page<Course> getAllCourses(Pageable pageable, CourseFilterForm filter, String search) {

		CourseSpecificationBuilder specification = new CourseSpecificationBuilder(filter, search);

		return repository.findAll(specification.build(), pageable);
	}

	public boolean isCourseExistsByName(String name) {
		return repository.existsByName(name);
	}

	public void createCourse(CourseFormForCreating form) {
		repository.save(form.toEntity());
	}

	public Course getCourseByID(short id) {
		return repository.findById(id).get();
	}

	public void updateCourse(short id, CourseFormForUpdating form) {
		Course entity = repository.findById(id).get();
		entity.setName(form.getName());
		entity.setPrice(form.getPrice());
		entity.setContent(form.getContent());
		repository.save(entity);
	}

	@Transactional
	public void deleteCourse(List<Short> ids) {
		repository.deleteByIdIn(ids);	
	}


}
