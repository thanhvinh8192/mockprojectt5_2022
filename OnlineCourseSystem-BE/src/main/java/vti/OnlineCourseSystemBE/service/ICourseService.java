package vti.OnlineCourseSystemBE.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;



import vti.OnlineCourseSystemBE.form.CourseFilterForm;

import vti.OnlineCourseSystemBE.form.CourseFormForCreating;
import vti.OnlineCourseSystemBE.form.CourseFormForUpdating;
import vti.OnlineCourseSystemBE.entity.Course;



public interface ICourseService {

	Page<Course> getAllCourses(Pageable pageable, CourseFilterForm filter, String search);

	boolean isCourseExistsByName(String name);

	void createCourse(CourseFormForCreating form);

	Course getCourseByID(short id);

	void updateCourse(short id, CourseFormForUpdating form);

	void deleteCourse(List<Short> ids);

}
