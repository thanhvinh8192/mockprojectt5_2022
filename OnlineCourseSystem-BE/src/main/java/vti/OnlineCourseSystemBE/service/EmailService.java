package vti.OnlineCourseSystemBE.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;
import vti.OnlineCourseSystemBE.entity.Account;
import vti.OnlineCourseSystemBE.repository.RegistrationAccountTokenRepository;
import vti.OnlineCourseSystemBE.repository.ResetPasswordTokenRepository;

@Service
public class EmailService implements IEmailService{

    @Autowired
    private IAuthService accountService;

    @Autowired
    private RegistrationAccountTokenRepository registrationAccountTokenRepository;

    @Autowired
    private ResetPasswordTokenRepository resetPasswordTokenRepository;

    @Autowired
    private JavaMailSender mailSender;

    @Override
    public void sendRegistrationAccountConfirmation(String email) {
        Account account = accountService.findAccountByEmail(email);
        String token = registrationAccountTokenRepository.findByAccountId(account.getId());
        String confirmationUrl = "http://localhost:8080/api/auth/active-account?token=" + token;
        String subject = "Xác Nhận Đăng Ký Account";
        String content = "Bạn đã đăng ký thành công. Click vào link dưới đây để kích hoạt tài khoản \n"
                + confirmationUrl;

        sendEmail(email, subject, content);
    }

    @Override
    public void sendResetPassword(String email) {
        Account account = accountService.findAccountByEmail(email);
        String token = resetPasswordTokenRepository.findByAccountId(account.getId());
        String confirmationUrl = "http://localhost:3000/auth/reset-password/" + token;
        String subject = "Reset Password Confirm";
        String content = "Click vào link dưới đây để reset password \n"
                + confirmationUrl;

        sendEmail(email, subject, content);
    }


    private void sendEmail (final String recipientEmail, final String subject, final String content) {
        SimpleMailMessage message = new SimpleMailMessage();
        message.setTo(recipientEmail);
        message.setSubject(subject);
        message.setText(content);

        mailSender.send(message);
    }
}
