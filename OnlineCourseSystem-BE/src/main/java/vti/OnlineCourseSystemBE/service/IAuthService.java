package vti.OnlineCourseSystemBE.service;


import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.userdetails.UserDetailsService;
import vti.OnlineCourseSystemBE.entity.Account;
import vti.OnlineCourseSystemBE.form.AccountFilterForm;


public interface IAuthService extends UserDetailsService {

    void registerAccount(Account account);

    void activeAccount(String token);

    Account findAccountByEmail(String email);

    void forgotPassword(String email);

    void resetPassword(String token, String newPassword);

    boolean existAccountByEmail(String email);

    boolean existAccountByUsername(String username);
}
