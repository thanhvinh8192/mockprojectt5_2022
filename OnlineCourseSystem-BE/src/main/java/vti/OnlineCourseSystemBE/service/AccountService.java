package vti.OnlineCourseSystemBE.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import vti.OnlineCourseSystemBE.entity.Account;
import vti.OnlineCourseSystemBE.form.AccountFilterForm;
import vti.OnlineCourseSystemBE.repository.AccountRepository;
import vti.OnlineCourseSystemBE.specification.AccountSpecification;

@Service
@Transactional
public class AccountService implements IAccountService {

    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;


    @Override
    public Page<Account> getPagingAccounts(Pageable pageable, String search, AccountFilterForm filterForm) {
        Specification<Account> where = AccountSpecification.buildWhere(search, filterForm);
        return accountRepository.findAll(where, pageable);
    }

    @Override
    public void createAccount(Account account) {
        account.setPassword(passwordEncoder.encode("123456"));
        account.setStatus(Account.AccountStatus.ACTIVE);
        accountRepository.save(account);
    }

    @Override
    public Account findAccountByUsername(String username) {
        return accountRepository.findByUsername(username);
    }

    public void updateAccountInfoByUsername(Account account) {
        accountRepository.save(account);
    }

    @Override
    public void changePassword(Account account, String newPassword) {
        account.setPassword(passwordEncoder.encode(newPassword));
        accountRepository.save(account);
    }

    @Override
    public void deleteAccount(String username) {
        Account account = accountRepository.findByUsername(username);
        accountRepository.delete(account);
    }


}
