package vti.OnlineCourseSystemBE.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import vti.OnlineCourseSystemBE.entity.Account;
import vti.OnlineCourseSystemBE.entity.RegistrationAccountToken;
import vti.OnlineCourseSystemBE.entity.ResetPasswordToken;
import vti.OnlineCourseSystemBE.event.OnSendRegistrationConfirmEmailEvent;
import vti.OnlineCourseSystemBE.event.OnSendResetPasswordConfirmEmailEvent;
import vti.OnlineCourseSystemBE.repository.AccountRepository;
import vti.OnlineCourseSystemBE.repository.RegistrationAccountTokenRepository;
import vti.OnlineCourseSystemBE.repository.ResetPasswordTokenRepository;

import java.util.UUID;

@Service
@Transactional
public class AuthService implements IAuthService {

    @Autowired
    private RegistrationAccountTokenRepository registrationAccountTokenRepository;

    @Autowired
    private ResetPasswordTokenRepository resetPasswordTokenRepository;

    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private ApplicationEventPublisher eventPublisher;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public UserDetailsImpl loadUserByUsername(String username) throws UsernameNotFoundException {

        Account account = accountRepository.findByUsername(username);

        if (account == null) throw new UsernameNotFoundException("Account Not Found with username: " + username);
        if (account.getRole() != null) {
            return new UserDetailsImpl( account.getUsername(),
                    account.getEmail(),
                    account.getFirstName(),
                    account.getLastName(),
                    account.getPassword(),
                    account.getStatus().toString(),
                    AuthorityUtils.createAuthorityList(account.getRole().toString())
            );
        }
        else {
            return new UserDetailsImpl( account.getUsername(),
                    account.getEmail(),
                    account.getFirstName(),
                    account.getLastName(),
                    account.getPassword(),
                    account.getStatus().toString(),
                    AuthorityUtils.createAuthorityList("STUDENT")
            );
        }
    }


    @Override
    public void registerAccount(Account account) {

        account.setPassword(passwordEncoder.encode(account.getPassword()));

        accountRepository.save(account);

        createRegistrationAccountToken(account);

        sendTokenViaEmail(account.getEmail());
    }

    private void createRegistrationAccountToken(Account account) {
        String newToken = UUID.randomUUID().toString();
        RegistrationAccountToken token = new RegistrationAccountToken(newToken, account);

        registrationAccountTokenRepository.save(token);
    }
    private void sendTokenViaEmail(String email) {
        eventPublisher.publishEvent(new OnSendRegistrationConfirmEmailEvent(email));
    }

    @Override
    public void activeAccount(String token) {
        RegistrationAccountToken registrationAccountToken = registrationAccountTokenRepository.findByToken(token);

        Account account = registrationAccountToken.getAccount();
        account.setStatus(Account.AccountStatus.ACTIVE);

        accountRepository.save(account);
        registrationAccountTokenRepository.deleteById(registrationAccountToken.getId());
    }

    //Send Email
    @Override
    public Account findAccountByEmail(String email) {
        return accountRepository.findByEmail(email);
    }

    @Override
    public void forgotPassword(String email) {

        Account account = findAccountByEmail(email);

        deleteResetPasswordByAccountId(account.getId());
        createResetPasswordToken(account);

        eventPublisher.publishEvent(new OnSendResetPasswordConfirmEmailEvent(account.getEmail()));
    }

    private void deleteResetPasswordByAccountId(int accountId) {
        resetPasswordTokenRepository.deleteByAccountId(accountId);
    }

    private void createResetPasswordToken(Account account) {
        String newToken = UUID.randomUUID().toString();
        ResetPasswordToken token = new ResetPasswordToken(newToken, account);

        resetPasswordTokenRepository.save(token);
    }

    @Override
    public void resetPassword(String token, String newPassword) {
        ResetPasswordToken resetPasswordToken = resetPasswordTokenRepository.findByToken(token);

        Account account = resetPasswordToken.getAccount();
        account.setPassword(passwordEncoder.encode(newPassword));
        accountRepository.save(account);

        deleteResetPasswordByAccountId(account.getId());
    }

    @Override
    public boolean existAccountByEmail(String email) {
        return accountRepository.existsByEmail(email);
    }

    @Override
    public boolean existAccountByUsername(String username) {
        return accountRepository.existsByUsername(username);
    }
}
