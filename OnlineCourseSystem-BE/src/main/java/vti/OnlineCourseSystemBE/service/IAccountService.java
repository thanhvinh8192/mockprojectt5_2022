package vti.OnlineCourseSystemBE.service;


import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import vti.OnlineCourseSystemBE.entity.Account;
import vti.OnlineCourseSystemBE.form.AccountFilterForm;

public interface IAccountService {

    Page<Account> getPagingAccounts(Pageable pageable, String search, AccountFilterForm filterForm);

    void createAccount(Account account);

    Account findAccountByUsername(String username);

    void updateAccountInfoByUsername(Account account);

    void changePassword(Account account, String newPassword);

    void deleteAccount(String username);
}
