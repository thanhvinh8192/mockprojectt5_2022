package vti.OnlineCourseSystemBE.form;

import lombok.Data;

@Data
public class CourseFilterForm {

	private int minTotalMember;

	private int maxTotalMember;

}
