package vti.OnlineCourseSystemBE.form;

import lombok.Data;

@Data
public class AccountUpdatingForm {

    private  String email;
    private String firstName;
    private String lastName;

}
