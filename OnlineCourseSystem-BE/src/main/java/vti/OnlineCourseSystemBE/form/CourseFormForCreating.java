package vti.OnlineCourseSystemBE.form;

import lombok.Data;
import vti.OnlineCourseSystemBE.entity.Course;



@Data
public class CourseFormForCreating {

	private String name;
	private String content;
	private int price;

	public Course toEntity() {
		return new Course(name,price,content);
	}

}
