package vti.OnlineCourseSystemBE.form;

import lombok.Data;

@Data
public class ChangePasswordForm {

    private String oldPassword;
    private String newPassword;
}
