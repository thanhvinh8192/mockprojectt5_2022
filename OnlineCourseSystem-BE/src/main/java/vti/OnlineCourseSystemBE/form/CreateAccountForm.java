package vti.OnlineCourseSystemBE.form;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class CreateAccountForm {

    private String username;
    private String firstName;
    private String lastName;
    private String email;
    private String role;
}
