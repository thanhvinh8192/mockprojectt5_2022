package vti.OnlineCourseSystemBE.form;


import lombok.Data;

@Data
public class CourseFormForUpdating {

	private String name;

	private int price;
	
	private String content;

	
}
