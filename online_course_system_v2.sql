DROP DATABASE IF EXISTS online_course_system2;

CREATE DATABASE online_course_system2;

USE online_course_system2;

DROP TABLE IF EXISTS 	`categories`;
CREATE TABLE IF NOT EXISTS `categories` (
	id 				INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
	category_name 	NVARCHAR(50) NOT NULL UNIQUE
);

DROP TABLE IF EXISTS 	`course`;
CREATE TABLE IF NOT EXISTS `course` (
	id 				INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
	course_name 	NVARCHAR(50) NOT NULL UNIQUE,
    course_price	INT	UNSIGNED NOT NULL,
    category_id 	NVARCHAR(50) NOT NULL,
    -- total_member 	INT UNSIGNED,
    content 		TEXT,
    created_date	DATETIME DEFAULT NOW(),
    CONSTRAINT fk_categories
    FOREIGN KEY (category_id) REFERENCES `categories`(id)
);

DROP TABLE IF EXISTS 	`lesson`;
CREATE TABLE IF NOT EXISTS `lesson` (
	id 				INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
	lesson_name 	NVARCHAR(50) NOT NULL,
    course_id 		INT UNSIGNED,
    content 		TEXT,
    CONSTRAINT fk_course
    FOREIGN KEY (course_id) REFERENCES `course`(id)
);


-- create table: Account
DROP TABLE IF EXISTS `account`;
CREATE TABLE `account`(
	id				INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    username		VARCHAR(50) NOT NULL UNIQUE,
	`password` 		VARCHAR(100) NOT NULL,
    first_name		NVARCHAR(50) NOT NULL,
    last_name		NVARCHAR(50) NOT NULL,
    email			NVARCHAR(50) NOT NULL UNIQUE,
    avatar_url		NVARCHAR(250),
    `role` 			ENUM('ADMIN','STUDENT','SUPPORT', 'LECTURER') NOT NULL DEFAULT 'STUDENT',
    `status`		TINYINT DEFAULT 0 -- 0: Not Active, 1: Active, 2: Block
);

-- create table: Order
DROP TABLE IF EXISTS `course_order`;
CREATE TABLE `course_order`(
	id				INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    account_id		INT UNSIGNED NOT NULL,
	course_id 		INT UNSIGNED NOT NULL,
    order_date 		DATETIME,
    payment_status	TINYINT DEFAULT 1, -- 0: Non-payment, 1: Recevied
    payment_date	DATETIME,
    
    CONSTRAINT fk_order_course FOREIGN KEY (course_id) REFERENCES `course`(id) 
		ON DELETE CASCADE 
		ON UPDATE CASCADE,
    CONSTRAINT fk_order_account FOREIGN KEY (account_id) REFERENCES `account`(id) 
		ON DELETE CASCADE 
		ON UPDATE CASCADE,
	UNIQUE KEY (account_id, course_id)
);

/*

DROP TABLE IF EXISTS 	`course_account`;
CREATE TABLE IF NOT EXISTS `course_account` (
	id 				INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
	course_id 		INT UNSIGNED,
    account_id		INT	UNSIGNED,
    mark 			TINYINT UNSIGNED,
    CONSTRAINT fk_course
    FOREIGN KEY (course_id) 
    REFERENCES `course`(id) 
		ON DELETE SET NULL 
        ON UPDATE CASCADE,
    CONSTRAINT fk_account 
    FOREIGN KEY (account_id) 
    REFERENCES `account`(id) 
		ON DELETE SET NULL 
		ON UPDATE CASCADE,
	CONSTRAINT `unique_key_course_account` UNIQUE (course_id,account_id)
);

*/

-- Create table Registration_Account_Token
DROP TABLE IF EXISTS `Registration_Account_Token`;
CREATE TABLE `Registration_Account_Token`(
	id 						INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    token 					CHAR(36) NOT NULL UNIQUE KEY,
	account_id				INT UNSIGNED NOT NULL,
    expiry_date				DATETIME NOT NULL,
    CONSTRAINT fk_register_account
    FOREIGN KEY (account_id) 
    REFERENCES `account`(id) 
		ON DELETE CASCADE 
		ON UPDATE CASCADE
);

-- Create table Reset_Password_Token
DROP TABLE IF EXISTS 	`Reset_Password_Token`;
CREATE TABLE IF NOT EXISTS `Reset_Password_Token` ( 	
	id 				INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
	`token`	 		CHAR(36) NOT NULL UNIQUE,
	`account_id` 	INT UNSIGNED NOT NULL,
	`expiryDate` 	DATETIME NOT NULL,
    CONSTRAINT fk_reset_password_account
    FOREIGN KEY (account_id)
    REFERENCES `account`(id) 
		ON DELETE CASCADE 
		ON UPDATE CASCADE
);