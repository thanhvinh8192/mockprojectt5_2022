
const setToken = (token) => {
    localStorage.setItem('token', token)
}

const getToken = (token) => {
    localStorage.getItem('token', token)
}

const setAccountInfo = (account) => {
    localStorage.setItem("username", account.username);
    localStorage.setItem("firstName", account.firstName);
    localStorage.setItem("lastName", account.lastName);
    localStorage.setItem("email", account.email);
    localStorage.setItem("role", account.role);
    localStorage.setItem("status", account.status);
}

const getAccountInfo = () => {
    return {
        "username": localStorage.getItem("username"),
        "firstName": localStorage.getItem("firstName"),
        "lastName": localStorage.getItem("lastName"),
        "email": localStorage.getItem("email"),
        "role": localStorage.getItem("role"),
        "status": localStorage.getItem("status"),
    };
}

const storage = {setToken, getToken, setAccountInfo, getAccountInfo}

export default storage