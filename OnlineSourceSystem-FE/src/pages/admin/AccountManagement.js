import React, { useEffect, useState } from "react";
import {
  Button,
  Card,
  CardHeader,
  CardBody,
  Col,
  Container,
  Row,
  FormGroup,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader
} from "reactstrap";
import { Formik, FastField, Form } from 'formik';
import * as Yup from 'yup';

import BootstrapTable from "react-bootstrap-table-next";
import paginationFactory from "react-bootstrap-table2-paginator";
import ToolkitProvider from 'react-bootstrap-table2-toolkit';
import { connect } from "react-redux";
import { selectAccounts, selectPage, selectSize, selectTotalSize } from "../../redux/selectors/AccountManagementSelector";
import { getListAccountsAction } from "../../redux/actions/accountManagementAction"
import AccountApi from "../../api/AccountApi";
import Auth from "../../api/Auth"
import CustomSearch from "./CustomSearch";
import { ReactstrapInput, ReactstrapSelect } from "reactstrap-formik";
import Select from "react-select";
import { Filter, RefreshCw,UserPlus , Trash2 } from "react-feather";
import { toastr } from "react-redux-toastr";

const options = [
  { value: "chocolate", label: "STUDENT" },
  { value: "strawberry", label: "LECTURER" },
  { value: "strawberry", label: "SUPPORT" },
  { value: "vanilla", label: "ADMIN" }
];

const tableColumns = [
  {
    dataField: "username",
    text: "Username",
    sort: true
  },
  {
    dataField: "firstName",
    text: "Firstname",
    sort: true
  },
  {
    dataField: "lastName",
    text: "Lastname",
    sort: true
  },
  {
    dataField: "email",
    text: "Email",
    sort: true
  },
  {
    dataField: "role",
    text: "Role",
    sort: true
  },
  {
    dataField: "status",
    text: "Status",
    sort: true
  },
  {
    dataField: "",
    text: "Action",
    sort: true
  }
];

const CreateAccountSchema = Yup.object().shape({
  firstName: Yup.string()
    .max(50, 'Must be less than 50 characters or equal')
    .required('Required'),

  lastName: Yup.string()
    .max(50, 'Must be less than 50 characters or equal')
    .required('Required'),

  username: Yup.string()
    .required('Required')
    .max(50, 'Must be between 6 to 50 characters')
    .min(6, 'Must be between 6 to 50 characters')
    .test('checkUniqueUsername', 'This username is already registered.', async username => {
      // call api
      const isExists = await Auth.existsByUsername(username);
      return !isExists;
    }),

  email: Yup.string()
    .required('Required')
    .max(50, 'Must be between 6 to 50 characters')
    .min(6, 'Must be between 6 to 50 characters')
    .email('Invalid email address')
    .test('checkUniqueEmail', 'This email is already registered.', async email => {
      const isExists = await Auth.existsByEmail(email);
      return !isExists;
    }),
  
  role: Yup.string()
    .ensure()
    .required("Role is required!")
})

const AccountManagement = (props) => {

  const [isOpenModal, setOpenModal] = useState(false);
  const getListAccounts = props.getListAccountsAction

  const size = props.size

  useEffect(() => {
    const getAllAccounts = async () => {
      const result = await AccountApi.getAllAccounts(1, size)
      const accounts = result.content;
      const totalSize = result.totalElements
      const sortField = result.totalElements
      const sortOrder = result.totalElements


      getListAccounts(accounts, 1, totalSize, sortField, sortOrder)
    }
    getAllAccounts()
  }, [getListAccounts])

  const handleTableChange = async (type, { page, sortField, sortOrder, searchText }) => {
    if (sortField === null || sortField === undefined || sortOrder === null || sortOrder === undefined) {
      sortField = 'id'
      sortOrder = 'desc'
    }
    const result = await AccountApi.getAllAccounts(page, size, sortField, sortOrder, searchText)
    const accounts = result.content
    const totalSize = result.totalElements

    getListAccounts(accounts, page, totalSize, sortField, sortOrder)
  }

  const showSuccessNotification = (title, message) => {
    const options = {
      timeOut: 2500,
      showCloseButton: false,
      progressBar: false,
      position: "top-right"
    };
    toastr.success(title, message, options);
  }

  return (
    <Container fluid className="p-0">

      <h1 className="h3 mb-3">Account Management Page</h1>
      
          <Card>
            <CardHeader>
              {/* <CardTitle tag="h5" className="mb-0">Empty card</CardTitle> */}
            </CardHeader>
            <CardBody>
              <ToolkitProvider
                keyField="name"
                data={props.accounts}
                columns={tableColumns}
                search
              >
                {
                  toolkitprops => (
                    <div>
                      <Row>
                        <Col lg="6">
                          <CustomSearch {...toolkitprops.searchProps} />
                        </Col>
                        <Col lg="3">
                            <FormGroup>
                            <Select
                              className="react-select-container"
                              classNamePrefix="react-select"
                              options={options}
                              isClearable
                            />
                          </FormGroup>
                        </Col>
                        <Col lg="3">
                          <div className="float-right pull-right">
                            <Filter size={24} onClick={() => console.log("Clicked")} />
                            <RefreshCw />
                            <UserPlus size="24" className="align-middle mr-2" onClick={() => setOpenModal(true)}/>
                            <Trash2/>
                          </div>
                        </Col>
                      </Row>
                      
                      <BootstrapTable
                        {...toolkitprops.baseProps}
                        remote
                        bootstrap4
                        bordered={false}
                        striped
                        hover
                        pagination={paginationFactory({
                          page: props.page,
                          sizePerPage: props.size,
                          totalSize: props.totalSize,
                          hideSizePerPage: true
                        })}
                        onTableChange={handleTableChange}
                      />
                    </div>
                  )
                }
              </ToolkitProvider>
            </CardBody>
          </Card>

          <Modal isOpen={isOpenModal}>
        <Formik
          initialValues={
            {
              username: '',
              firstName: '',
              lastName: '',
              email: '',
              role:'',
              errorForm: ''
            }
          }
          validationSchema={CreateAccountSchema}
          validateOnChange={false}
          validateOnBlur={false}
          onSubmit={
            async (values) => {
              try {
                // call api
                await AccountApi.createAccount(
                                  values.username,
                                  values.firstName,
                                  values.lastName,
                                  values.email,
                                  values.role
                                )

                setOpenModal(false);
                // show notification
                showSuccessNotification(
                  "Create Account",
                  "Create Account Successfully!");
                // reload group page


              } catch (error) {
                console.log(error);
                setOpenModal(false);
              }
            }
          }
          
        >
          {({ isSubmitting }) => (
            <Form>
              <ModalHeader> Create Account </ModalHeader>

              <ModalBody className="m-3">
                <Row style={{ alignItems: "center" }}>
                  <Col>

                  <FormGroup>
                    <FastField
                      label="First Name"
                      bsSize="lg"
                      type="text"
                      name="firstname"
                      placeholder="Enter your first name"
                      component={ReactstrapInput}
                    />
                  </FormGroup>

                  <FormGroup>
                    <FastField
                      label="Last Name"
                      bsSize="lg"
                      type="text"
                      name="lastname"
                      placeholder="Enter your last name"
                      component={ReactstrapInput}
                    />
                  </FormGroup>

                  <FormGroup>
                    <FastField
                      label="Username"
                      bsSize="lg"
                      type="text"
                      name="username"
                      placeholder="Enter your username"
                      component={ReactstrapInput}
                    />
                  </FormGroup>

                  {/* email */}
                  <FormGroup>
                    <FastField
                      label="Email"
                      bsSize="lg"
                      type="email"
                      name="email"
                      placeholder="Enter your email"
                      component={ReactstrapInput}
                    />
                  </FormGroup>

                  <FormGroup>
                    <FastField
                      label="Role"
                      bsSize="lg"
                      name="role"
                      component={ReactstrapSelect}
                      inputprops={{
                        name: "role",
                        id: "role",
                        options: ["STUDENT", "LECTURER", "ADMIN", "SUPPORT"],
                        defaultOption: "ROLE"
                      }}
                    />
                  </FormGroup>
                  </Col>
                </Row>
              </ModalBody>
 
              <ModalFooter>
                <Button
                  color="primary"
                  style={{ marginLeft: 10 }}
                  disabled={isSubmitting}
                  type="submit"
                >
                  Create
                </Button>
                <Button
                  color="primary"
                  onClick={() => setOpenModal(false)}
                >
                  Cancel
                </Button>
              </ModalFooter>
            </Form>
          )}
        </Formik>
      </Modal>

    </Container>
  )
}

const mapGlobalStateToProps = state => {
  return {
    accounts: selectAccounts(state),
    page: selectPage(state),
    size: selectSize(state),
    totalSize: selectTotalSize(state)
  }
}

export default connect(mapGlobalStateToProps, { getListAccountsAction })(AccountManagement);
