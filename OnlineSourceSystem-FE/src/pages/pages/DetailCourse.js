import React from 'react'
import { Button, Card, CardBody, CardImg, CardLink, CardSubtitle, CardText, CardTitle, Col, Container, Row } from 'reactstrap'
import {BsFillPeopleFill} from "react-icons/bs";
import "./DetailCoure.css"

export default function DetailCourse(props) {

  const { state } = props.location
  console.log(state)
  return (

    
    <div>
        <Container fluid className="p-0">
    <Row>
        <Col xl ="11">
        <h1 className="h3 mb-3">{state.name}</h1>
        </Col>
        <Col xl ="1">  
        <span> {state.totalMember} <BsFillPeopleFill style={{fontSize:"x-large"}}/></span>
        </Col>

    </Row>
    <Row>
      <Col xl = "8">
        <div className='Right'>
        <Card>
    <CardBody>
      <CardTitle tag="h5" className='Title_name'>
      Lesson 1
      </CardTitle>
      <CardSubtitle
        className="mb-2 text-muted"
        tag="h6"
      >
        Introduce of {state.name}
      </CardSubtitle>
    </CardBody>
    <img
      alt="Card image cap"
      src="https://picsum.photos/318/180"
      width="100%"
    />
    <CardBody>
      <CardText>
        {state.content}
      </CardText>
      <Button>
         Register
      </Button>
      
    </CardBody>
  </Card>

        </div>
      </Col>
      <Col xl ="4">  
      <div className='Left'>
      <Card
    body
    inverse
    style={{
      backgroundColor: '#333',
      borderColor: '#333'
    }}
  >
    <CardTitle tag="h5" className="title">
     Lesson 2
    </CardTitle>
    <CardText>
      With supporting text below as a natural lead-in to additional content.
    </CardText>
    
  </Card>
  <Card
    body
    color="primary"
    inverse
  >
    <CardTitle tag="h5" className="title">
    Lesson 3
    </CardTitle>
    <CardText>
      With supporting text below as a natural lead-in to additional content.
    </CardText>
 
  </Card>
  <Card
    body
    color="success"
    inverse
  >
    <CardTitle tag="h5" className="title">
    Lesson 4
    </CardTitle>
    <CardText>
      With supporting text below as a natural lead-in to additional content.
    </CardText>

  </Card>
  <Card
    body
    color="info"
    inverse
  >
    <CardTitle tag="h5" className="title">
    Lesson 5
    </CardTitle>
    <CardText>
      With supporting text below as a natural lead-in to additional content.
    </CardText>
  </Card>
  <Card
    body
    color="warning"
    inverse
  >
    <CardTitle tag="h5" className="title">
    Lesson 6
    </CardTitle>
    <CardText>
      With supporting text below as a natural lead-in to additional content.
    </CardText>
  </Card>
  <Card
    body
    color="danger"
    inverse
  >
    <CardTitle tag="h5" className='title'>
    Lesson 7
    </CardTitle>
    <CardText>
      With supporting text below as a natural lead-in to additional content.
    </CardText>
  </Card>
  <Card
    body
    color="primary"
    inverse
  >
    <CardTitle tag="h5" className='title'>
    Lesson 8
    </CardTitle>
    <CardText>
      With supporting text below as a natural lead-in to additional content.
    </CardText>
  </Card>
      </div>

      </Col>
    </Row>
    <Row>
      
    </Row>

    </Container>
    </div>
  )
}
