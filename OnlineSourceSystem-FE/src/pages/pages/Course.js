import React, { useEffect, useRef, useState } from 'react'
import { Button, Card, CardImg, CardText, CardTitle, Col, Container, Row } from 'reactstrap'
import { AiFillFilter,AiOutlinePlusCircle } from "react-icons/ai";
import { TbSortAscending } from "react-icons/tb";
import {BsFillPeopleFill} from "react-icons/bs";
import {GiSpanner} from "react-icons/gi";
import api from '../../api/CourseApi';
import "./Course.css"
import {
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
} from "reactstrap";
import { Formik, FastField, Form ,Field} from 'formik';
import * as Yup from 'yup';
import { ReactstrapInput } from "reactstrap-formik";
import { Link } from 'react-router-dom';


export default function Course() {
   const [courses, setCourse] = useState([])
   const [isOpenModalFilter, setOpenModalFilter] = useState(false);
   const [isOpenModalCreate, setOpenModalCreate] = useState(false);
   const [isOpenModalUpdate, setOpenModalUpdate] = useState(false);
   const [courseUpdate,setCourseUpdate] = useState(
     {}
   )
   const [isReset, setReset] = useState(true)
   const [isSort, setSort] = useState(false)
   const [data, setData] = useState(
     { sort: null,
      type: "asc",
      minTotalMember: null,
      maxTotalMember: null,
      search: null

     })
   const [isRole, setRole] = useState("")
   const inputEl = useRef(null);


  const UpdateCourse = (id,name,price,content) => {
    setCourseUpdate({
      id,
      name,
      price,
      content
    })
    setOpenModalUpdate(true)
    

  }

  const SearchCourse = () => {
     console.log(inputEl.current.value)
     async function asyncCall() {
      const result = await api.getAll(1,10,data.sort,data.type,inputEl.current.value,data.minTotalMember,data.maxTotalMember);
      setCourse(result.content)
      setData({
        ...data,
        search:inputEl.current.value
      })
      // expected output: "resolved"
    }
    asyncCall()

   }

   const SortingCouse = () => {
      setSort(!isSort)
      const sorter = isSort ? "desc" : "asc"

    async function asyncCall() {
      const result = await api.getAll(1,10,"name",sorter,inputEl.current.value,data.minTotalMember,data.maxTotalMember);
   
      setCourse(result.content)

      
   }
   asyncCall()
  }
    
    
    useEffect(() => {
      const role = localStorage.getItem('role');
      if (role !== null && role !== undefined && role == '[ADMIN]') {
        setRole('ADMIN')
        
        console.log(isRole)
    } else if (role !== null && role !== undefined && role == '[LECTURER]') {
      setRole('LECTURER')
    }
  


        async function asyncCall() {
            const result = await api.getAll();
            setCourse(result.content)
           
            // expected output: "resolved"
          }
          asyncCall()
      },[isRole,isReset])

  return (
    <Container fluid className="p-0">
    <Row>
        <Col xl ="8" className='TitleContainer'>
        <h1 className="h3 mb-3">Course</h1>
        <span><AiOutlinePlusCircle  style={isRole == "ADMIN" ? {fontSize:"24px", marginLeft:"10px"} : {display:"none"}} onClick= {() => setOpenModalCreate(true)} /></span>
        </Col>
        <Col xl ="4">  
            <AiFillFilter style={{fontSize:"24px"}} className={isOpenModalFilter ? "active" : " "} onClick= {() => setOpenModalFilter(true)}/>
            <TbSortAscending style={{fontSize:"24px", marginLeft:"10px", }} className={isSort ? "active" : " "} onClick={SortingCouse} />
            <input type="text" style={{width:"200px", marginLeft:"10px"}} ref={inputEl} />
            <Button style={{marginLeft:"10px"}}
                color="primary"
                onClick={SearchCourse}>
                Search
            </Button>
        </Col>
    </Row>
    

    <Row style={{marginTop:"10px"}}>
      <Col xl="12">
      <Row>
        {courses.map((course) => (
            <>
                    <Col sm="4">
                    <Card body className='card'>
                    <CardTitle tag="h5">
                    <Link className='linked'
                          to={{
                            pathname: "DetailCourse",
                            state: {
                              name: course.name,
                              picture: "https://picsum.photos/318/180",
                              content: course.content,
                              price: course.price,
                              totalMember: course.totalMember
                            }
                          // your data array of objects
                          }}
                        >
                            
                              {course.name}
                         
                    </Link>
                    {isRole == "LECTURER" ? <GiSpanner  style={{margin:"0 0 5px 10px"}} onClick={() => UpdateCourse(course.id,course.name,course.price,course.content)}/> : ""}
                    </CardTitle>

                   
                    <CardImg
                        alt="Card image cap"
                        src="https://picsum.photos/318/180"
                        top
                        width="100%"
            />
                    <CardText>
                        <div>
                            <div className="top">{course.content}</div> 
                            <div className="down">
                                <span className='down_price'>$ {course.price}</span>
                                <span> {course.totalMember ? course.totalMember : "0"} <BsFillPeopleFill/></span>
                                
                            </div>
                        </div>
                    </CardText>
                    <Button>
                        Register
                    </Button>
                    </Card>
                </Col>
              
            </>
        ))}
        
    </Row>
      </Col>
      <Col xl="12">
        
      </Col>
    </Row>
      {/* Modal Filter*/}
    <Modal
        isOpen={isOpenModalFilter}
      >
        <Formik
          initialValues={
            {
              minTotalMember: "",
              maxTotalMember:"",
              sort: "emty",
            }
          }
          validationSchema={
            Yup.object({
              minTotalMember: Yup.number()
                .required('Required')
                .max(100,'Must less than 100' )
                .min(1, 'Must larger than 0'),

              maxTotalMember: Yup.number()
              .required('Required')
              .max(100, 'Must less than 100')
              .min(1, 'Must larger than 0'),

            })
          }
          onSubmit={
            async (values) => {
              console.log(values.sort)
              try {
                if(values.sort == "emty") {
                  const result = await api.getAll(1,10,null,null,data.search,values.minTotalMember,values.maxTotalMember);
                  setCourse(result.content)
                  setData(
                    {...data,
                     sort: null,
                      type: "asc",
                      minTotalMember: values.minTotalMember,
                      maxTotalMember: values.maxTotalMember,               
                })
                     console.log(data)
                }
              
                const result = await api.getAll(1,10,values.sort,"asc",data.search,values.minTotalMember,values.maxTotalMember);
                
                setCourse(result.content)
                setData(
                  {...data,
                   sort: values.sort,
                    type: "asc",
                    minTotalMember: values.minTotalMember,
                    maxTotalMember: values.maxTotalMember,               
                   })
                
                setOpenModalFilter(false);
                // show notification

              } catch (error) {
                console.log(error);
                setOpenModalFilter(false);
              }
            }
          }
          validateOnChange={false}
          validateOnBlur={false}
        >
          {({ isSubmitting }) => (
            <Form>
              {/* header */}
              <ModalHeader>
                Filter
              </ModalHeader>

              {/* body */}
              <ModalBody className="m-3">

                {/* Name */}
                <Row style={{ alignItems: "center" }}>
                  <Col xs="4">
                    Min TotalMember:
                  </Col>
                  <Col xs="2">
                    <FastField
                      bsSize="lg"
                      type="number"
                      name="minTotalMember"
                      placeholder=""
                      component={ReactstrapInput}
                    />
                  </Col>
                  <Col xs="4">
                  Max TotalMember:
                    </Col>
                  <Col xs="2">
                    <FastField
                      bsSize="lg"
                      type="number"
                      name="maxTotalMember"
                      placeholder=""
                      component={ReactstrapInput}
                    />
                  </Col>
                  <Col xs="2">
                  Sort by:
                    </Col>
                  <Col xs="4">
                  <Field as="select" name="sort" >
                  <option value="emty"></option>        
                    <option value="name">Course Name</option>
                    <option value="price">Price</option>
                  </Field>
                  </Col>
                </Row>
              </ModalBody>

              {/* footer */}
              <ModalFooter>
                {/* resend */}
                <Button
                  color="primary"
                  style={{ marginLeft: 10 }}
                  disabled={isSubmitting}
                  type="submit"
                >
                  Save
                </Button>

                {/* close button */}
                <Button
                  color="primary"
                  onClick={() => setOpenModalFilter(false)}
                >
                  Cancel
                </Button>
              </ModalFooter>
            </Form>
          )}
        </Formik>
      </Modal>

    {/* Modal Create*/}
    <Modal
        isOpen={isOpenModalCreate}
      >
        <Formik
          initialValues={
            {
              name: "",
              price:"",
              content: "",
            }
          }
          validationSchema={
            Yup.object({
              name: Yup.string()
                .required('Required')
                .max(30,'Must less than 30' )
                .min(1, 'Must larger than 0'),

              price: Yup.number()
              .required('Required')
              .max(900000, 'Must less than 900000')
              .min(1, 'Must larger than 0'),

              content: Yup.string()
              .required('Required')
              .max(1000, 'Must less than 1000')
              .min(10, 'Must larger than 10'),
            })
          }
          onSubmit={
            async (values) => {
             
              try {
               
                   await api.create(values.name,values.price,values.content);
                   setReset(!isReset)
                 
                
                setOpenModalCreate(false);
                // show notification

              } catch (error) {
                console.log(error);
                setOpenModalCreate(false);
              }
            }
          }
          validateOnChange={false}
          validateOnBlur={false}
        >
          {({ isSubmitting }) => (
            <Form>
              {/* header */}
              <ModalHeader>
                Create Course
              </ModalHeader>

              {/* body */}
              <ModalBody className="m-3">

                {/* Name */}
                <Row style={{ alignItems: "center" }}>
                  <Col xs="2">
                    Name 
                  </Col>
                  <Col xs="4">
                    <FastField
                      bsSize="lg"
                      type="text"
                      name="name"
                      placeholder=""
                      component={ReactstrapInput}
                    />
                  </Col>
                  <Col xs="2">
                  Price
                    </Col>
                  <Col xs="4">
                    <FastField
                      bsSize="lg"
                      type="number"
                      name="price"
                      placeholder=""
                      component={ReactstrapInput}
                    />
                  </Col>
                  <Col xs="12">
                    Content :
                    </Col>
                  <Col xs="12">
                    <FastField style={{width: "100%", height:"90px"}}
                    bsSize="lg"
                    type="text"
                    name="content"
                    placeholder=""
                    component={ReactstrapInput}

                    />
                  
                  </Col>
                </Row>
              </ModalBody>

              {/* footer */}
              <ModalFooter>
                {/* resend */}
                <Button
                  color="primary"
                  style={{ marginLeft: 10 }}
                  disabled={isSubmitting}
                  type="submit"
                >
                  Save
                </Button>

                {/* close button */}
                <Button
                  color="primary"
                  onClick={() => setOpenModalCreate(false)}
                >
                  Cancel
                </Button>
              </ModalFooter>
            </Form>
          )}
        </Formik>
      </Modal>

      {/* Modal Update*/}
      <Modal
        isOpen={isOpenModalUpdate}
      >
        <Formik
          initialValues={
            {
              name: courseUpdate.name,
              price:courseUpdate.price,
              content: courseUpdate.content,
            }
          }
          validationSchema={
            Yup.object({
              name: Yup.string()
                .required('Required')
                .max(30,'Must less than 30' )
                .min(1, 'Must larger than 0'),

              price: Yup.number()
              .required('Required')
              .max(900000, 'Must less than 900000')
              .min(1, 'Must larger than 0'),

              content: Yup.string()
              .required('Required')
              .max(1000, 'Must less than 1000')
              .min(10, 'Must larger than 10'),
            })
          }
          onSubmit={
            async (values) => {
              
              try {
                await api.update(courseUpdate.id,values.name,values.price,values.content)
                setReset(!isReset)
               
                 
                
                setOpenModalUpdate(false);
                // show notification

              } catch (error) {
                console.log(error);
                setOpenModalUpdate(false);
              }
            }
          }
          validateOnChange={false}
          validateOnBlur={false}
        >
          {({ isSubmitting }) => (
            <Form>
              {/* header */}
              <ModalHeader>
                Create Course
              </ModalHeader>

              {/* body */}
              <ModalBody className="m-3">

                {/* Name */}
                <Row style={{ alignItems: "center" }}>
                  <Col xs="2">
                    Name 
                  </Col>
                  <Col xs="4">
                    <FastField
                      bsSize="lg"
                      type="text"
                      name="name"
                      placeholder=""
                      component={ReactstrapInput}
                    />
                  </Col>
                  <Col xs="2">
                  Price
                    </Col>
                  <Col xs="4">
                    <FastField
                      bsSize="lg"
                      type="number"
                      name="price"
                      placeholder=""
                      component={ReactstrapInput}
                    />
                  </Col>
                  <Col xs="12">
                    Content :
                    </Col>
                  <Col xs="12">
                    <FastField style={{width: "100%", height:"90px"}}
                    bsSize="lg"
                    type="text"
                    name="content"
                    placeholder=""
                    component={ReactstrapInput}

                    />
                  
                  </Col>
                </Row>
              </ModalBody>

              {/* footer */}
              <ModalFooter>
                {/* resend */}
                <Button
                  color="primary"
                  style={{ marginLeft: 10 }}
                  disabled={isSubmitting}
                  type="submit"
                >
                  Save
                </Button>

                {/* close button */}
                <Button
                  color="primary"
                  onClick={() => setOpenModalUpdate(false)}
                >
                  Cancel
                </Button>
              </ModalFooter>
            </Form>
          )}
        </Formik>
      </Modal>
  </Container>

  
  )
}
