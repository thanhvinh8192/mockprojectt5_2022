import { useState } from "react";
import React from "react";

import {
  Button,
  Card,
  CardBody,
  FormGroup,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader
} from "reactstrap";
import { Formik, FastField, Form, ErrorMessage } from 'formik';
import { ReactstrapInput } from "reactstrap-formik";
import * as Yup from 'yup';
import Auth from "../../api/Auth";


const ResetPasswordSchema = Yup.object().shape({
  email: Yup.string()
    .required('Required')
    .max(50, 'Must be between 6 to 50 characters')
    .min(6, 'Must be between 6 to 50 characters')
    .email('Invalid email address')
    .test('checkEmailExists', 'This email is not exists.',
      async email => {
        const isExists = await Auth.existsByEmail(email)
        return isExists
      })
})

const ForgotPassword = (props) => {

  const [isOpenModal, setOpenModal] = useState(false)
  const [email, setEmail] = useState("")

  const handleCloseModel = () => {
    setOpenModal(false);
    props.history.push("/auth/sign-in")
  }
  return (
    <>
      <div className="text-center mt-4">
        <h1 className="h2">Forgot password</h1>
        <p className="lead">Enter your email to reset your password.</p>
      </div>

      <Formik
        initialValues={
          {
            email: '',
            errorForm: ''
          }
        }
        validationSchema={ResetPasswordSchema}
        validateOnChange={false}
        validateOnBlur={false}
        onSubmit={
          async (values, { setFieldError }) => {
            try {
              await Auth.requestResetPassword(values.email); 
              setOpenModal(true);
              setEmail(values.email);

            } catch (error) {
              setFieldError('errorForm', 'There is an error from the server');
              console.log(error);
            }
          }
        }
        
      >
        {({ isSubmitting }) => (
          <Card>
            <CardBody>
              <div className="m-sm-4">
                <Form>
                  <FormGroup>
                    <FastField
                      label="Email"
                      bsSize="lg"
                      type="email"
                      name="email"
                      placeholder="Enter your email"
                      component={ReactstrapInput}
                    />
                  </FormGroup>

                  <ErrorMessage name="errorForm" component={"div"} className="invalid-feedback" style={{ display: "block" }} />

                  <div className="text-center mt-3">
                    <Button type="submit" color="primary" size="lg" disabled={isSubmitting}>
                      Reset password
                  </Button>
                  </div>
                </Form>
              </div>
            </CardBody>
          </Card>
        )}
      </Formik>

      <Modal isOpen={isOpenModal}>
        <ModalHeader>
          You need to confirm reset password
        </ModalHeader>

        
        <ModalBody className="m-3">
          <p>
            We have sent an email to <b>{email}</b>.
          </p>
          <p>
            Please check your email to reset password.
          </p>
        </ModalBody>

        <ModalFooter> 
          <Button
            type="submit"
            color="primary"
            onClick={handleCloseModel}
          >
            Login
          </Button>
        </ModalFooter>
      </Modal>
    </>
  )
};




export default ForgotPassword;
