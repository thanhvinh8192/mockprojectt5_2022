import React from "react";
import "./SignUp.css";
import { Formik, Form, ErrorMessage } from 'formik';
import * as Yup from 'yup';
import {Button, Card, Label, CardBody, FormGroup, Input, Modal, ModalBody, ModalFooter, ModalHeader } from "reactstrap";
import Auth from "../../api/Auth"
import { useState } from "react";


const SignUpSchema = Yup.object().shape({
  firstName: Yup.string()
    .max(50, 'Must be less than 50 characters or equal')
    .required('Required'),

  lastName: Yup.string()
    .max(50, 'Must be less than 50 characters or equal')
    .required('Required'),

  username: Yup.string()
    .required('Required')
    .max(50, 'Must be between 6 to 50 characters')
    .min(6, 'Must be between 6 to 50 characters')
    .test('checkUniqueUsername', 'This username is already registered.', async username => {
      // call api
      const isExists = await Auth.existsByUsername(username);
      return !isExists;
    }),

  email: Yup.string()
    .required('Required')
    .max(50, 'Must be between 6 to 50 characters')
    .min(6, 'Must be between 6 to 50 characters')
    .email('Invalid email address')
    .test('checkUniqueEmail', 'This email is already registered.', async email => {
      // call api
      const isExists = await Auth.existsByEmail(email);
      return !isExists;
    }),

  password: Yup.string()
    .max(50, 'Must be between 6 to 50 characters')
    .min(6, 'Must be between 6 to 50 characters')
    .required('Required'),

  confirmPassword: Yup.string()
    .when("password", {
      is: value => (value && value.length > 0 ? true : false),
      then: Yup.string().oneOf(
        [Yup.ref("password")],
        "Both password need to be the same"
      )
    })
    .required('Required')
})

const SignUp = (props) => {

  const [isOpenModal, setOpenModal] = useState(false)
  const [email, setEmail] = useState("")

  const handleCloseModel = () => {

    setOpenModal(false);

    props.history.replace("/auth/sign-in");
  }

  return (
    <>
      <div className="text-center mt-4">
        <h1 className="h2">Sign Up</h1>
        <p className="lead">
          Register account for Online Course System
        </p>
      </div>

      <Formik
        initialValues={
          {
            username: '',
            firstName: '',
            lastName: '',
            email: '',
            password: '',
            confirmPassword: '',
            errorForm: ''
          }
        }

        validationSchema={SignUpSchema}

        onSubmit={
          async (values, { setFieldError }) => {
            try {
              // call api
              await Auth.registerAccount(
                values.username,
                values.firstName,
                values.lastName,
                values.email,
                values.password
                )

              // open model 
              setOpenModal(true);
              setEmail(values.email);
              
            } catch (error) {
              setFieldError('errorForm', 'There is an error from the server');
              console.log(error);
            }
          }
        }

        validateOnChange={false}
        validateOnBlur={false}
      >
        {props => {
          const {
            values,
            touched,
            errors,
            isSubmitting,
            handleChange,
            handleBlur,
            handleSubmit
          } = props
          return (
            <Card>
              <CardBody>
                <div className="m-sm-4">
                  <Form onSubmit={handleSubmit}>

                    <FormGroup>
                      <Label>Username</Label>
                      <Input
                        bsSize="lg"
                        type="text"
                        name="username"
                        value={values.username}
                        onChange={handleChange}
                        onBlur={handleBlur}
                        placeholder="thanhvinh"
                        className={errors.username && touched.username ? "text-input error" : "text-input"}
                      />
                      {errors.username && touched.username && (<div className="input-feedback">{errors.username}</div>)}
                    </FormGroup>

                    <FormGroup>
                      <Label>First Name</Label>
                      <Input
                        bsSize="lg"
                        type="text"
                        name="firstName"
                        value={values.firstName}
                        onChange={handleChange}
                        onBlur={handleBlur}
                        placeholder="Nguyen Thanh"
                        className={errors.firstName && touched.firstName ? "text-input error" : "text-input"}
                      />
                      {errors.firstName && touched.firstName && (<div className="input-feedback">{errors.firstName}</div>)}
                    </FormGroup>

                    <FormGroup>
                      <Label>Last Name</Label>
                      <Input
                        bsSize="lg"
                        type="text"
                        name="lastName"
                        value={values.lastName}
                        onChange={handleChange}
                        onBlur={handleBlur}
                        placeholder="Vinh"
                        className={errors.lastName && touched.lastName ? "text-input error" : "text-input"}
                      />
                      {errors.lastName && touched.lastName && (<div className="input-feedback">{errors.lastName}</div>)}
                    </FormGroup>

                    <FormGroup>
                      <Label>Email</Label>
                      <Input
                        bsSize="lg"
                        type="email"
                        name="email"
                        value={values.email}
                        onChange={handleChange}
                        onBlur={handleBlur}
                        placeholder="account@onlinecoursesystem.com"
                        className={errors.email && touched.email ? "text-input error" : "text-input"}
                      />
                      {errors.email && touched.email && (<div className="input-feedback">{errors.email}</div>)}
                    </FormGroup>

                    <FormGroup>
                      <Label>Password</Label>
                      <Input
                        bsSize="lg"
                        type="password"
                        name="password"
                        value={values.password}
                        onChange={handleChange}
                        onBlur={handleBlur}
                        placeholder="Enter your password"
                        className={errors.password && touched.password ? "text-input error" : "text-input"}
                      />
                      {errors.password && touched.password && (<div className="input-feedback">{errors.password}</div>)}
                    </FormGroup>

                    <FormGroup>
                      <Label>Confirm Password</Label>
                      <Input
                        bsSize="lg"
                        type="password"
                        name="confirmPassword"
                        value={values.confirmPassword}
                        onChange={handleChange}
                        onBlur={handleBlur}
                        placeholder="Confirm your password"
                        className={errors.confirmPassword && touched.confirmPassword ? "text-input error" : "text-input"}
                      />
                      {errors.confirmPassword && touched.confirmPassword && (<div className="input-feedback">{errors.confirmPassword}</div>)}
                    </FormGroup>

                    <ErrorMessage name="errorForm" component={"div"} className="invalid-feedback" style={{ display: "block" }} />

                    <div className="text-center mt-3">
                      <Button type="submit" color="primary" size="lg" disabled={isSubmitting}>
                        Sign up
                      </Button>
                    </div>
                  </Form>
                </div>
              </CardBody>
            </Card>
          )
        }}
      </Formik>

      <Modal isOpen={isOpenModal}>

        <ModalHeader className="text-center">
          You need to confirm your account
        </ModalHeader>

        <ModalBody className="m-3">
          <p>
            We have sent an email to <b>{email}</b>.
          </p>
          <p>
            Please check your email to active account.
          </p>
        </ModalBody>

        <ModalFooter>
          {/* <Button
            color="primary"
            onClick={resendEmailToActiveAccount}
            style={{ marginLeft: 10 }}
          disabled={isDisabledResendEmailButton}
          >
            Resend
          </Button> */}
          <Button
            color="primary"
            onClick={handleCloseModel}
          >
            Login
          </Button>
        </ModalFooter>
      </Modal>

    </>
  )
};



export default SignUp;
