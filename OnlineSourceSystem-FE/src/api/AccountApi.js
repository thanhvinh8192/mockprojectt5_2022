import Api from "./API"

const url = "account";

const getAllAccounts = (page=1, size=10, sortField="id", sortType="desc", search = '') => {

    const parameters = {
        pageNumber: page,
        size,
        sort: `${sortField},${sortType}`
        
    }
    if (search) {
        parameters.search = search
    }
    return Api.get(`${url}/paging-account`, {params: parameters})
}


const createAccount = (username, firstName, lastName, email, password, role) => {
    const body = {
        username, //username (same Json in backend)
        firstName,
        lastName,
        email,
        password,
        role
    }
    return Api.post(`${url}/create-account`, body);
};

// export
const AccountApi = { getAllAccounts, createAccount }
export default AccountApi;