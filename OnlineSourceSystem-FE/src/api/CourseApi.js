import Api from "./API"

const url = "/courses";

const getAll = (page, size, sortField, sortType, search, minTotalMember, maxTotalMember) => {

    // default parameters
    if (sortField === null || sortField === undefined || sortType === null || sortType === undefined) {
        sortField = "id";
        sortType = "desc";
    }

    const requestParams = {
        page,
        size,
        sort: `${sortField},${sortType}`,
        search,
        minTotalMember,
        maxTotalMember
    }

    if (minTotalMember) {
        requestParams.minTotalMember = minTotalMember;
    }

    if (maxTotalMember) {
        requestParams.maxTotalMember = maxTotalMember;
    }

    return Api.get(url, { params: requestParams });
};
const existsByName = (name) => {
    return Api.get(`${url}/name/${name}`);
};

const create = (name,price,content) => {
    const body = {
        name,
        price,
        content
    };

    return Api.post(url, body);
};

const getByID = (id) => {
    return Api.get(`${url}/${id}`);
};

const update = (id, name,price,content) => {
    const body = {
        name,
        price,
        content
    }
    return Api.put(`${url}/${id}`, body);
};

const deleteByIds = (ids) => {
    return Api.delete(`${url}/${ids.toString()}`);
};

// export
const api = { getAll, existsByName, create, update, getByID, deleteByIds }
export default api;