import axios from 'axios';

const axiosClient = axios.create({
    baseURL: `http://localhost:8080/api/`,
    responseType: 'json'
});

axiosClient.interceptors.request.use(async(config) => {
    const token = localStorage.getItem('token');
    if (token !== null && token !== undefined) {
        config.headers.Authorization = `Bearer ${token}`;
    }

    return config;
});


axiosClient.interceptors.response.use((response) => {
    if (response && response.data !== undefined) {
        // only get data
        return response.data;
    }

    return response;
}, (error) => {
    // Handle errors
    throw error;
});

export default axiosClient;