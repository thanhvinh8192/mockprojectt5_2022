import Api from "./API"

const url = "auth";

const existsByEmail = (email) => {
    return Api.get(`${url}/email/${email}`);
};

const existsByUsername = (username) => {
    return Api.get(`${url}/username/${username}`);
};

const registerAccount = (userName, firstname, lastname, email, password) => {
    const body = {
        username: userName, //username (same Json in backend)
        firstName: firstname,
        lastName: lastname,
        email: email,
        password: password
    }
    return Api.post(`${url}/sign-up`, body);
};

const resendEmailToActiveAccount = (email) => {
    const requestParams = {
        email: email
    }

    return Api.get(`${url}/userRegistrationConfirmRequest`, { params: requestParams });
};

const signIn = (username, password) => {

    const body = {
        username: username, //username (same Json in backend)
        password: password
    }

    return Api.post(`${url}/sign-in`, body);
};

const requestResetPassword = (email) => {
    const requestParams = {
        email: email
    }

    return Api.get(`${url}/forgot-password`, { params: requestParams });
};

const resetPassword = (token, newPassword) => {
    const requestParams = {
        token: token,
        newPassword: newPassword
    }

    return Api.get(`${url}/reset-password`, { params: requestParams });
};

// export
const api = { existsByEmail, existsByUsername, registerAccount, resendEmailToActiveAccount, signIn, requestResetPassword, resetPassword }
export default api;