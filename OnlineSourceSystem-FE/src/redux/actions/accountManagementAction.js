import AccountApi from "../../api/AccountApi"
import * as types from "../constants"

// const listAccountsAction = (accounts)=> {
//     return {
//         type: types.ACCOUNT_MANAGEMENT,
//         payload: accounts
//     }
// }

// export const getListAccountsAction = () => {
//     return async dispatch => {
//         try {
//             const json = await AccountApi.getAllAccounts()
//             const accounts = json.data.content
//             dispatch(listAccountsAction(accounts))
//             console.log(accounts)
//         }
//         catch (error) {
//             console.log(error)
//         }
//     }
// }

export const getListAccountsAction = (accounts, page, totalSize, sortField, sortType) => {
    return {
        type: types.ACCOUNT_MANAGEMENT,
        payload: {
            accounts,
            page,
            totalSize,
            sortField,
            sortType
        }
    }

}