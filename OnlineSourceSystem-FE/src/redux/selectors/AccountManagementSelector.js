import { createSelector } from "@reduxjs/toolkit"

const accountManagementSelector = (state) => state.Account

const selectAccountSelector = createSelector(
    accountManagementSelector,
    state => state.accounts)

const selectPageSelector = createSelector(
    accountManagementSelector,
    state => state.page)

const selectSizeSelector = createSelector(
    accountManagementSelector,
    state => state.size)

const selectTotalSizeSelector = createSelector(
    accountManagementSelector,
    state => state.totalSize)

// function
export const selectAccounts = (state) => {
    return selectAccountSelector(state)
}

export const selectPage = (state) => {
    return selectPageSelector(state)
}

export const selectSize = (state) => {
    return selectSizeSelector(state)
}

export const selectTotalSize = (state) => {
    return selectTotalSizeSelector(state)
}
