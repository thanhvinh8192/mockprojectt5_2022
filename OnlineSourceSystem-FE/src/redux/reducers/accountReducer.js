import * as types from "../constants";

const initialState = {
    accounts: [],
    page: 1,
    size: 5,
    totalSize: 0,
    sortField: "id",
    sortType: "desc"
}

export default function reducer(state = initialState, actions) {
    switch (actions.type) {
        case types.ACCOUNT_MANAGEMENT:
            return {
                ...state,
                accounts: actions.payload.accounts,
                //paging
                page: actions.payload.page,
                totalSize: actions.payload.totalSize,
                //sorting
                sortField: actions.payload.sortField,
                sortType: actions.payload.sortType
            }
        default:
            return state
    }
}