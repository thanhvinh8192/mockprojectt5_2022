
function CustomInputComponent({
    field,
    form: {touched, errors},
    ...props
}) {
    return (
        <div>
            <label htmlFor={field.name}>{props.label}</label>
            <input {...field} {...props}/>
            {touched[field.name] && <div>{errors[field.name]}</div>}
        </div>
    )
}

export default CustomInputComponent